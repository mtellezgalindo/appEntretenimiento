//
//  CollectionViewController.swift
//  SlideMenuControllerSwift
//
//  Created by Martin on 20/04/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import UIKit

//let reuseIdentifier = "Cells"

class CollectionViewController: UICollectionViewController, NSXMLParserDelegate {

  /*  @IBOutlet weak var labelText: UILabel!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    var parseo: NSXMLParser = NSXMLParser()
    var cellPosts: [CellPost] = []
    var cellTitle: String = String()
    var cellLink: String = String()
    var cellImage: String = String()
    var eName: String = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        super.viewDidLoad()
       // configureTableView()
        let url:NSURL = NSURL(string: "http://feeds.esmas.com/data-feeds-esmas/sports/articles_single_entdummy.xml")!
        parseo = NSXMLParser(contentsOfURL: url)!
        parseo.delegate = self
        parseo.parse()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [NSObject : AnyObject]) {
    eName = elementName
    if elementName == "item" {
        cellTitle = String()
        cellLink = String()
        cellImage = String()
    }
}

func parser(parser: NSXMLParser, foundCharacters string: String?) {
    let data = string!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    if (!data.isEmpty) {
        if eName == "title" {
            cellTitle += data
        } else if eName == "link" {
            cellLink += data
        } else if eName == "media:thumbnail"{
            cellImage += data
        }
    }
}

func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
    if elementName == "item" {
        let cellPost: CellPost = CellPost()
        cellPost.cellTitle = cellTitle
        cellPost.cellLink = cellLink
        cellPost.cellImage = cellImage
        cellPosts.append(cellPost)
    }
}
    /*func configureTableView() {
        collectionView.row
        tableView.rowHeight = 600
    }*/

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
    }
    
    
    */
    
    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
        //return 0
        return cellPosts.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cells", forIndexPath: indexPath) as UICollectionViewCell
    
        let cellPost: CellPost = cellPosts[indexPath.row]
        
        // Configure the cell

       
       /* cell.label?.text = cellPost.cellTitle
    
        
        cell.textLabel?.text = cellPost.cellTitle
        cell.textLabel?.numberOfLines = 4
        cell.textLabel?.lineBreakMode = .ByWordWrapping
        cell.textLabel?.textColor = UIColor.blueColor()
        cell.textLabel?.font = UIFont.systemFontOfSize(15.0)
        
        if(cell.selected){
            cell.backgroundColor = UIColor.whiteColor()
            cell.textLabel?.textColor = UIColor.whiteColor()
        }else{
            cell.backgroundColor = UIColor.whiteColor()
            cell.textLabel?.textColor = UIColor.blackColor()
        }*/

    
        return cell
    }*/

   
}
