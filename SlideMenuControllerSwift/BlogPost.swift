//
//  BlogPost.swift
//  SlideMenuControllerSwift
//
//  Created by Martin on 15/04/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import Foundation

class BlogPost {
    
    var postTitle: String = String()
    var postLink: String = String()
    var postImage: String = String()
}