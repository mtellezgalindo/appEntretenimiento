//
//  LeftViewController.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 12/3/14.
//

import UIKit

enum LeftMenu: Int {
    case Main = 0
    case Swift
    case Java
    case Go
    case Read
    case Cuadro
    case NonMenu
}

protocol LeftMenuProtocol : class {
    func changeViewController(menu: LeftMenu)
}

class LeftViewController : UIViewController, LeftMenuProtocol {
    
    
    
    @IBOutlet weak var tableView: UITableView!
    var menus = ["Main", "Swift", "Java", "Go", "NonMenu", "Cuadros","Espectaculos"]
    var mainViewController: UIViewController!
    var swiftViewController: UIViewController!
    var javaViewController: UIViewController!
    var goViewController: UIViewController!
    var nonMenuViewController: UIViewController!
    var brtableViewController: UINavigationController!
    var cuadroViewController : UINavigationController!
    
    override init() {
        super.init()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.tableView.separatorColor = UIColor(red: 226/255, green: 181/255, blue: 40/255, alpha: 0.5)

        self.tableView.backgroundView = UIImageView(image: UIImage(named: "backmenu.png"))
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        let swiftViewController = storyboard.instantiateViewControllerWithIdentifier("SwiftViewController") as SwiftViewController
        self.swiftViewController = UINavigationController(rootViewController: swiftViewController)
        
        let javaViewController = storyboard.instantiateViewControllerWithIdentifier("JavaViewController") as JavaViewController
        self.javaViewController = UINavigationController(rootViewController: javaViewController)
        
        let goViewController = storyboard.instantiateViewControllerWithIdentifier("GoViewController") as GoViewController
        self.goViewController = UINavigationController(rootViewController: goViewController)
        
        
        let readTableView = storyboard.instantiateViewControllerWithIdentifier("BRTableViewController") as BRTableViewController
        self.brtableViewController = UINavigationController(rootViewController: readTableView)
        
        let readCuadrosView = storyboard.instantiateViewControllerWithIdentifier("CollectionViewControllerController") as
            CollectionViewController
        self.cuadroViewController = UINavigationController(rootViewController: readCuadrosView)
        
        
        let nonMenuController = storyboard.instantiateViewControllerWithIdentifier("NonMenuController") as NonMenuController
        nonMenuController.delegate = self
        self.nonMenuViewController = UINavigationController(rootViewController: nonMenuController)
        
        self.tableView.registerCellClass(BaseTableViewCell.self)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: BaseTableViewCell = BaseTableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: BaseTableViewCell.identifier)
        cell.backgroundColor = UIColor.clearColor()
        cell.textLabel?.font = UIFont (name: "Helvetica Neue", size: 20)
        cell.textLabel?.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
        cell.textLabel?.text = menus[indexPath.row]
        let colorView = UIView()
        colorView.backgroundColor = UIColor.yellowColor()
        cell.selectedBackgroundView = colorView

        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.item) {
            self.changeViewController(menu)
        }
    }
    
    func changeViewController(menu: LeftMenu) {
        switch menu {
        case .Main:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .Swift:
            self.slideMenuController()?.changeMainViewController(self.swiftViewController, close: true)
            break
        case .Java:
            self.slideMenuController()?.changeMainViewController(self.javaViewController, close: true)
            break
        case .Go:
            self.slideMenuController()?.changeMainViewController(self.goViewController, close: true)
            break
        case .NonMenu:
            self.slideMenuController()?.changeMainViewController(self.brtableViewController, close: true)
            break
        case .Cuadro:
            self.slideMenuController()?.changeMainViewController(self.cuadroViewController, close: true)
            break
        case .Read:
            self.slideMenuController()?.changeMainViewController(self.nonMenuViewController, close: true)
        default:
            break
        }
    }
    
}